# zigzag
software for develop the chined hull surface of a sailing boat. (stitch and glue construction) 
HISTORIC 

for chined hull surface develop.
develop surface made by two curves, the chines of the hull.

howto use ZigZag.

./zigzag [tipo] [intervals] [curveA_3d] [curveB_3d]


[tipo] kind of interpolation
     curvaA  curvaB
AA   akima   akima
AL   akima   linear
LA   linear  akima
LL   linear  linear

intervals - number of elements in the mesh.
curveA - file.txt with the points 3d of curve A.
curveB -                                curve B.
its recomended the use of major dimension as x
always initialize and finalize both the curves with the same x.
  

need of gsl library instaled.
linked with -lgsl -lgslcblas

CURVES FORMAT:
X   Y   Z

RESULTS: 2 lines of the developed surface
curvaA.dat
curvaB.dat
/***************************************************************************
 *   Copyright (C) 2008 by Janos Erzinger - Brasil                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


// ZigZag - versao beta - software de desenvolvimento de superficie.
// autor - Janos Erzinger
// Copyleft pela lei Brasileira.
// Uso permitido sem fins lucrativos.
// SOFTWARE LIVRE - lei Brasileira de incentivo.
//   algoritimo - foi aplicada uma malha triangular com vertices sobre as linhas que
//   definem a superficie, calculados os angulos e construida a superficie no plano.
// compilar com
// necessita gsl - gnu scientific library
// c++ -O0 -g3 -Wall -o zigzag zigzag.cpp -largtable2 -lgsl -lgslcblas
// uso:  ./zigzag [numero de intervalos] [arquivo A] [arquivo B]

// junho -2013
// modificação - inicializa as splines linear e akima, fazendo uso mediante
// sinalização pelo argumento de entrada.
// uso:  ./zigzag [tipo] [numero de intervalos] [arquivo A] [arquivo B]
// tipos AA akima akima
// tipos LL linear linear
//       AL akima linear
//       LA linear akima



#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <cstdlib>
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <math.h>

#include <cstring>

//#include <argtable2.h> later implementation

#include <time.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

using namespace std;



#define numero_de_controles 100    // num.of spline controls at gsl

int flag_debug = 0;
// 1 imprime saidas no terminal.
// 0 não imprime.

int flag_A=1;  // tipo interpolacao curva A
int flag_B=1;  // tipo interpolacao curva B
// 1 - akima
// 2 - linear
// TODO - falta gerar Varkon-file

// numero de controles dos arquivos de entrada

double curva_Ax[numero_de_controles];
double curva_Ay[numero_de_controles];
double curva_Az[numero_de_controles];

double curva_Bx[numero_de_controles];
double curva_By[numero_de_controles];
double curva_Bz[numero_de_controles];

/************************************/
// Leitura dos controles
/***********************************/

float xi, yi, zi;

char arquivo_A[40];
char arquivo_B[40];

char tipo[4]; // flag pro tipo de interpolação

int nctrls_A;			// numero de controles da curva A
void le_controle_curva_A()
{
	int i = 0;
	double ajuste=0.000000000001;
	FILE *streamA;
	streamA = fopen(arquivo_A, "r");
	while (fscanf(streamA, "%f %f %f", &xi, &yi, &zi) != EOF) {

		if(i==0) yi=yi+ajuste; // AJUSTE NO CASO DE INICIO PONTO IGUAL!!

		curva_Ax[i] = xi;
		curva_Ay[i] = yi;
		curva_Az[i] = zi;
		i++;
	}
	curva_Ay[i]=curva_Ay[i]+ajuste; // AJUSTE NO CASO DE FINAL PONTO IGUAL!!
	nctrls_A = i;
	fclose(streamA);
}

int nctrls_B;			// numero de controles da curva B
void le_controle_curva_B()
{
	int i = 0;
	FILE *streamB;
	streamB = fopen(arquivo_B, "r");
	while (fscanf(streamB, "%f %f %f", &xi, &yi, &zi) != EOF) {
		curva_Bx[i] = xi;
		curva_By[i] = yi;
		curva_Bz[i] = zi;
		i++;
	}
	nctrls_B = i;
	fclose(streamB);
}

//=====================================================
// FUNCOES EM VETORES COM INICIO NA ORIGEM
// angulo entre dois vetores com inicio na origem !
// o valor pode ser aplicado diretamente nas funcoes

// a curva A é a curva de base [origem]
// a curva B é a curva superior

inline double cos_angulo_vetor(double x1, double y1, double z1, double x2,
		double y2, double z2)
{

	return (((x1 * x2) + (y1 * y2) + (z1 * z2)) /
			(sqrt((x1 * x1) + (y1 * y1) + (z1 * z1)) *
					sqrt((x2 * x2) + (y2 * y2) + (z2 * z2))));
}

inline double comprimento_vetor(double x, double y, double z)
{
	return (sqrt((x * x) + (y * y) + (z * z)));
}

//=====================================================

inline double modulo(double valor)
{
	return (sqrt(valor * valor));
}


//====================================
//====================================
//      ELEMENTO
//
//       curva B
//
//     _ C____D_
//      |.    |
//      | .   |
//      |  .  |
//      |   . |
//    _ A_____B_
//
//       curva A
//
//====================

double Ax, Ay, Az;
double Aox, Aoy, Aoz;	// na origem
double Bx, By, Bz;
double Box, Boy, Boz;	// na origem

double Cx, Cy, Cz;
double Cox, Coy, Coz;

double Dx, Dy, Dz;
double Dox, Doy, Doz;	// na origem

//==========================================
//==========================================

int main(int argc, char *argv[])
{



	if (strncmp(argv[1], "--help", 2)==0){
		printf("uso:  ./zigzag [tipo] [numero de intervalos] [arquivo A] [arquivo B]\n ");
		printf("tipo AA akima akima\n ");
		printf("tipo LL linear linear\n ");
		printf("tipo AL akima linear\n ");
		printf("tipo LA linear akima\n ");
		return(0);
	}



	if (strncmp(argv[1], "AA", 2)==0)
	{
		printf("\n  tipo AA \n");
		(flag_A=1);
		(flag_B=1);
	}

	if (strncmp(argv[1], "LL", 2)==0){
		printf("\n  tipo LL \n");
		(flag_A=2);
		(flag_B=2);
	}
	if (strncmp(argv[1], "AL", 2)==0){
		printf("\n  tipo AL \n");
		(flag_A=1);
		(flag_B=2);
	}
	if (strncmp(argv[1], "LA", 2)==0){
		printf("\n  tipo LA \n");
		(flag_A=2);
		(flag_B=1);
	}



	//========================
	//      ELEMENTO
	//
	//       curva B
	//
	//     _ C____D_
	//      |.    |
	//      | .   |
	//      |  .  |
	//      |   . |
	//    _ A_____B_
	//
	//       curva A
	//
	//====================


	strcat(arquivo_A, argv[3]);
	strcat(arquivo_B, argv[4]);

	clock_t inicio, fim;

	inicio = clock();

	FILE *stream_curvaA;
	stream_curvaA = fopen("curvaA.dat", "w");

	FILE *stream_curvaB;
	stream_curvaB = fopen("curvaB.dat", "w");

	//---------------------------------------------------
	// finalidade de plotagem 3d(gnuplot)

	FILE *stream_curvaA3d;
	stream_curvaA3d = fopen("curvaA3d.dat", "w");

	FILE *stream_curvaB3d;
	stream_curvaB3d = fopen("curvaB3d.dat", "w");
	//---------------------------------------------------

	FILE *stream_constroi;
	stream_constroi = fopen("constroi.txt", "w");

	FILE *stream_elementos;
	stream_elementos = fopen("elementos.txt", "w");


	// interpolação Akima
	//======================= SPLINE A =====================
	le_controle_curva_A();
	//=======================================================



	gsl_interp_accel *acc_Axy = gsl_interp_accel_alloc();
	gsl_spline *spline_Axy = gsl_spline_alloc(gsl_interp_akima, nctrls_A);
	gsl_spline_init(spline_Axy, curva_Ax, curva_Ay, nctrls_A);
	//=========================
	gsl_interp_accel *acc_Axz = gsl_interp_accel_alloc();
	gsl_spline *spline_Axz = gsl_spline_alloc(gsl_interp_akima, nctrls_A);
	gsl_spline_init(spline_Axz, curva_Ax, curva_Az, nctrls_A);
	//======================================================

	//======================= SPLINE B =====================
	le_controle_curva_B();
	//=======================================================
	gsl_interp_accel *acc_Bxy = gsl_interp_accel_alloc();
	gsl_spline *spline_Bxy = gsl_spline_alloc(gsl_interp_akima, nctrls_B);
	gsl_spline_init(spline_Bxy, curva_Bx, curva_By, nctrls_B);
	//=========================
	gsl_interp_accel *acc_Bxz = gsl_interp_accel_alloc();
	gsl_spline *spline_Bxz = gsl_spline_alloc(gsl_interp_akima, nctrls_B);
	gsl_spline_init(spline_Bxz, curva_Bx, curva_Bz, nctrls_B);
	//======================================================


	// interpolação linear
	//======================= SPLINE A -linear =====================
	gsl_interp_accel *Lacc_Axy = gsl_interp_accel_alloc();
	gsl_spline *Lspline_Axy = gsl_spline_alloc(gsl_interp_linear, nctrls_A);
	gsl_spline_init(Lspline_Axy, curva_Ax, curva_Ay, nctrls_A);
	//=========================
	gsl_interp_accel *Lacc_Axz = gsl_interp_accel_alloc();
	gsl_spline *Lspline_Axz = gsl_spline_alloc(gsl_interp_linear, nctrls_A);
	gsl_spline_init(Lspline_Axz, curva_Ax, curva_Az, nctrls_A);
	//======================================================

	//======================= SPLINE B -linear =====================
	gsl_interp_accel *Lacc_Bxy = gsl_interp_accel_alloc();
	gsl_spline *Lspline_Bxy = gsl_spline_alloc(gsl_interp_linear, nctrls_B);
	gsl_spline_init(Lspline_Bxy, curva_Bx, curva_By, nctrls_B);
	//=========================
	gsl_interp_accel *Lacc_Bxz = gsl_interp_accel_alloc();
	gsl_spline *Lspline_Bxz = gsl_spline_alloc(gsl_interp_linear, nctrls_B);
	gsl_spline_init(Lspline_Bxz, curva_Bx, curva_Bz, nctrls_B);
	//======================================================

	double x;

	//===========================================================
	//           PLANIFICACAO
	//===========================================================
	// OBSERVACAO
	//  X DEVE SER IGUAL NAS DUAS CURVAS.

	int numero_elementos = strtol(argv[2], NULL, 10);

	double delta;
	delta =
			modulo(curva_Ax[0] - curva_Ax[nctrls_A - 1]) / numero_elementos;

	double l_curvaA = 0;
	double l_curvaB = 0;


	double L_AB;
	double L_CD;

	double L_AC;

	double L_CB;
	int i = 0;

	// angulos do elemento em radianos
	double alfa = 0;
	double beta = 0;
	double gama = 0;
	double ro = 0;

	// PONTOS PLANIFICADOS
	double AAx = 0;		// ponto curva A planificada
	double AAy = 0;
	double BBx = 0;		// ponto curva B planificada
	double BBy = 0;

	double limite_superior = curva_Ax[nctrls_A - 1];


	//  limite inferior   limite superior              incremento
	for (x = curva_Ax[0]; x <= limite_superior; x = x + delta) {

		if (flag_debug == 1)
			printf("x = %f\n", x);

		// ponto A em 3d.

		Ax = x;

		if(flag_A==1){
			Ay = gsl_spline_eval(spline_Axy, x, acc_Axy);
			Az = gsl_spline_eval(spline_Axz, x, acc_Axz);
			// ponto B em 3d.
			Bx = x + delta;
			By = gsl_spline_eval(spline_Axy, Bx, acc_Axy);
			Bz = gsl_spline_eval(spline_Axz, Bx, acc_Axz);
		}

		if(flag_A==2){
			Ay = gsl_spline_eval(Lspline_Axy, x, Lacc_Axy);
			Az = gsl_spline_eval(Lspline_Axz, x, Lacc_Axz);
			// ponto B em 3d.
			Bx = x + delta;
			By = gsl_spline_eval(Lspline_Axy, Bx, Lacc_Axy);
			Bz = gsl_spline_eval(Lspline_Axz, Bx, Lacc_Axz);
		}


		// posição de A na origem
		Box = Bx - Ax;
		Boy = By - Ay;
		Boz = Bz - Az;
		L_AB = comprimento_vetor(Box, Boy, Boz);

		// calculado o comprimento da curva A
		if (i != numero_elementos)
			l_curvaA = l_curvaA + L_AB;

		Cx = x;
		if(flag_B==1){
			Cy = gsl_spline_eval(spline_Bxy, x, acc_Bxy);
			Cz = gsl_spline_eval(spline_Bxz, x, acc_Bxz);

			Dx = x + delta;
			Dy = gsl_spline_eval(spline_Bxy, Dx, acc_Bxy);
			Dz = gsl_spline_eval(spline_Bxz, Dx, acc_Bxz);
		}

		if(flag_B==2){
			Cy = gsl_spline_eval(Lspline_Bxy, x, Lacc_Bxy);
			Cz = gsl_spline_eval(Lspline_Bxz, x, Lacc_Bxz);

			Dx = x + delta;
			Dy = gsl_spline_eval(Lspline_Bxy, Dx, Lacc_Bxy);
			Dz = gsl_spline_eval(Lspline_Bxz, Dx, Lacc_Bxz);
		}

		// posição de C na origem
		Dox = Dx - Cx;
		Doy = Dy - Cy;
		Doz = Dz - Cz;
		L_CD = comprimento_vetor(Dox, Doy, Doz);

		// calculado o comprimento da curva B
		if (i != numero_elementos)
			l_curvaB = l_curvaB + L_CD;

		// C com A na origem
		Cox = Cx - Ax;
		Coy = Cy - Ay;
		Coz = Cz - Az;
		L_AC = comprimento_vetor(Cox, Coy, Coz);

		// B com C na origem
		Box = Bx - Cx;
		Boy = By - Cy;
		Boz = Bz - Cz;
		L_CB = comprimento_vetor(Box, Boy, Boz);

		//=================================================

		// cos alfa com C na origem - angulo entre AC e BC
		Aox = Ax - Cx;
		Aoy = Ay - Cy;
		Aoz = Az - Cz;
		Box = Bx - Cx;
		Boy = By - Cy;
		Boz = Bz - Cz;
		alfa = acos(cos_angulo_vetor(Aox, Aoy, Aoz, Box, Boy, Boz));

		//---------------------------------------------------

		// cos beta com B na origem - angulo entre BC e BD
		Cox = Cx - Bx;
		Coy = Cy - By;
		Coz = Cz - Bz;
		Dox = Dx - Bx;
		Doy = Dy - By;
		Doz = Dz - Bz;
		beta = acos(cos_angulo_vetor(Cox, Coy, Coz, Dox, Doy, Doz));

		// no primeiro elemento, gama =0

		// saida
		fprintf(stream_curvaA, " %f       %f \n ", AAx, AAy);
		fprintf(stream_constroi, " %f       %f \n ", AAx, AAy);

		// saida 3d(gnuplot)
		fprintf(stream_curvaA3d, " %f       %f  0\n ", AAx, AAy);

		BBy = AAy + (L_AC * cos(gama));
		BBx = AAx + (L_AC * sin(gama));

		// saida
		fprintf(stream_curvaB, " %f       %f \n ", BBx, BBy);
		fprintf(stream_constroi, " %f       %f \n ", BBx, BBy);

		// saida 3d(gnuplot)
		fprintf(stream_curvaB3d, " %f       %f  0\n ", BBx, BBy);

		ro = alfa - gama;
		AAy = BBy - (L_CB * cos(ro));
		AAx = BBx + (L_CB * sin(ro));

		gama = beta - alfa;	// radianos

		// resultados por elemento
		fprintf(stream_elementos, "elemento       =  %d\n", i + 1);
		fprintf(stream_elementos, "delta          =  %f\n", delta);
		fprintf(stream_elementos, "x inicial      =  %f\n", x);
		fprintf(stream_elementos, "comprimento AC = %f\n", L_AC);
		fprintf(stream_elementos, "alfa graus     = %f\n",
				alfa * 180 / M_PI);
		fprintf(stream_elementos, "comprimento BC = %f\n", L_CB);
		fprintf(stream_elementos, "beta graus     = %f\n\n",
				beta * 180 / M_PI);

		i++;

	}

	if (flag_debug == 1)
		printf("\n  numero de elementos = %d\n", i - 1);

	// ========================================================
	// elemento faltante
	if (i - 1 != numero_elementos) {

		x = limite_superior;
		if (flag_debug == 1)
			printf("x = %f\n", x);

		// ponto A em 3d.

		Ax = x;
		if(flag_A==1){
			Ay = gsl_spline_eval(spline_Axy, x, acc_Axy);
			Az = gsl_spline_eval(spline_Axz, x, acc_Axz);
		}

		if(flag_A==2){
			Ay = gsl_spline_eval(Lspline_Axy, x, Lacc_Axy);
			Az = gsl_spline_eval(Lspline_Axz, x, Lacc_Axz);
		}

		// ponto B em 3d.
		Bx = x + delta;
		if(flag_A==1){
			By = gsl_spline_eval(spline_Axy, Bx, acc_Axy);
			Bz = gsl_spline_eval(spline_Axz, Bx, acc_Axz);
		}

		if(flag_A==2){
			By = gsl_spline_eval(Lspline_Axy, Bx, Lacc_Axy);
			Bz = gsl_spline_eval(Lspline_Axz, Bx, Lacc_Axz);
		}

		// posição de A na origem
		Box = Bx - Ax;
		Boy = By - Ay;
		Boz = Bz - Az;
		L_AB = comprimento_vetor(Box, Boy, Boz);

		// calculado o comprimento da curva A
		//l_curvaA = l_curvaA + L_AB;

		Cx = x;
		if(flag_B==1){
			Cy = gsl_spline_eval(spline_Bxy, x, acc_Bxy);
			Cz = gsl_spline_eval(spline_Bxz, x, acc_Bxz);
		}

		if(flag_B==2){
			Cy = gsl_spline_eval(Lspline_Bxy, x, Lacc_Bxy);
			Cz = gsl_spline_eval(Lspline_Bxz, x, Lacc_Bxz);
		}

		Dx = x + delta;
		if(flag_B==1){
			Dy = gsl_spline_eval(spline_Bxy, Dx, acc_Bxy);
			Dz = gsl_spline_eval(spline_Bxz, Dx, acc_Bxz);
		}

		if(flag_B==2){
			Dy = gsl_spline_eval(Lspline_Bxy, Dx, Lacc_Bxy);
			Dz = gsl_spline_eval(Lspline_Bxz, Dx, Lacc_Bxz);
		}

		// posição de C na origem
		Dox = Dx - Cx;
		Doy = Dy - Cy;
		Doz = Dz - Cz;
		L_CD = comprimento_vetor(Dox, Doy, Doz);

		// calculado o comprimento da curva B
		//l_curvaB = l_curvaB + L_CD;

		// C com A na origem
		Cox = Cx - Ax;
		Coy = Cy - Ay;
		Coz = Cz - Az;
		L_AC = comprimento_vetor(Cox, Coy, Coz);

		// B com C na origem
		Box = Bx - Cx;
		Boy = By - Cy;
		Boz = Bz - Cz;
		L_CB = comprimento_vetor(Box, Boy, Boz);

		//=================================================

		// cos alfa com C na origem - angulo entre AC e BC
		Aox = Ax - Cx;
		Aoy = Ay - Cy;
		Aoz = Az - Cz;
		Box = Bx - Cx;
		Boy = By - Cy;
		Boz = Bz - Cz;
		alfa = acos(cos_angulo_vetor(Aox, Aoy, Aoz, Box, Boy, Boz));

		//---------------------------------------------------

		// cos beta com B na origem - angulo entre BC e BD
		Cox = Cx - Bx;
		Coy = Cy - By;
		Coz = Cz - Bz;
		Dox = Dx - Bx;
		Doy = Dy - By;
		Doz = Dz - Bz;
		beta = acos(cos_angulo_vetor(Cox, Coy, Coz, Dox, Doy, Doz));

		// no primeiro elemento, gama =0

		// saida
		fprintf(stream_curvaA, " %f       %f \n ", AAx, AAy);
		fprintf(stream_constroi, " %f       %f \n ", AAx, AAy);

		fprintf(stream_curvaA3d, " %f       %f  0\n ", AAx, AAy);

		BBy = AAy + (L_AC * cos(gama));
		BBx = AAx + (L_AC * sin(gama));

		// saida
		fprintf(stream_curvaB, " %f       %f \n ", BBx, BBy);
		fprintf(stream_constroi, " %f       %f \n ", BBx, BBy);

		fprintf(stream_curvaB3d, " %f       %f  0\n ", BBx, BBy);

	}
	//===========================================================

	gsl_spline_free(spline_Axy);
	gsl_interp_accel_free(acc_Axy);
	gsl_spline_free(spline_Axz);
	gsl_interp_accel_free(acc_Axz);

	gsl_spline_free(spline_Bxy);
	gsl_interp_accel_free(acc_Bxy);
	gsl_spline_free(spline_Bxz);
	gsl_interp_accel_free(acc_Bxz);



	gsl_spline_free(Lspline_Axy);
	gsl_interp_accel_free(Lacc_Axy);
	gsl_spline_free(Lspline_Axz);
	gsl_interp_accel_free(Lacc_Axz);

	gsl_spline_free(Lspline_Bxy);
	gsl_interp_accel_free(Lacc_Bxy);
	gsl_spline_free(Lspline_Bxz);
	gsl_interp_accel_free(Lacc_Bxz);

	fim = clock();

	if (flag_debug == 1)
		cout << endl << "execução normal  " << (fim - inicio) << endl;

	fprintf(stream_elementos,
			"\ncomprimento curva A = %f     curva B = %f\n", l_curvaA,
			l_curvaB);

	if (flag_debug == 1)
		printf("\ncomprimento curva A = %f     curva B = %f\n", l_curvaA,
				l_curvaB);

	fclose(stream_curvaA);
	fclose(stream_curvaB);
	fclose(stream_constroi);
	fclose(stream_elementos);

	fclose(stream_curvaA3d);
	fclose(stream_curvaB3d);
}